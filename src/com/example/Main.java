package com.example;
public class Main {
    public static final String[] CABBAGE = {
            " .-~~~~-.",
            " /  ( ( ' \\ \\ ",
            "| ( )   )  |",
            " \\ ) ' }  / /",
            "(` \\ , /  ~)",
            "`-.`\\/_.-'",
            "  `\"\""
    };
    public static final String[] GOAT = {
            "_))",
            "> *\\     _~",
            "`;'\\\\__-' \\_",
            "  | )  _ \\ \\",
            " / / ``   w w"
    };
    public static final String[] WOLF = {
            "                 /\\__/\\  ",
            "                /  \\    /  ",
            "               |  |     | ",
            "   __________| \\     /|   ",
            "/              \\ T / |     ",
            "/                      |     ",
            "|  ||     |    |       /     ",
            "|  ||    /______\\     /    ",
            "|  | \\  |  /     \\   /  ",
            "\\/   | |\\ \\      | | ",
            "| | \\ \\     | |  \\ \\",
            "| |  \\ \\    | |   \\ \\",
            "'''   '''   '''    '''"
    };
    public static void main(String[] args) {
        System.out.println("Task!");
        System.out.println("Need to move on a Goat, a Cabbage and a Wolf to another side of the river.");
        System.out.println("But there is a problem: Goat want some vegetables, and Wolf want some meat!");
        System.out.println("Let`s start!");
        System.out.println("________________________________________________________________________________");
        System.out.println("Step 1");
        System.out.println("We need to transit a Goat! Wolf and Cabbage can not do troubles with each other.");
        drawImage(CABBAGE);
        System.out.println("CABBAGE - STAY HERE!");
        drawImage(GOAT);
        System.out.println("GOAT - GO TO THE BOAT!");
        drawImage(WOLF);
        System.out.println("WOLF - STAY HERE!");
        System.out.println("________________________________________________________________________________");
        System.out.println("Step 2");
        System.out.println("After returning we take to the boat a Wolf!");
        drawImage(CABBAGE);
        System.out.println("CABBAGE - STAY HERE!");
        drawImage(GOAT);
        System.out.println("GOAT HAS ALREADY ON ANOTHER SIDE!");
        drawImage(WOLF);
        System.out.println("WOLF - GO TO THE BOAT!");
        System.out.println("And... He ate us.:`( Moral: Do not take hungry predators on boat!");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
    public static void drawImage(String[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}
